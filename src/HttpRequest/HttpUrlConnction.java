package HttpRequest ;



import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import httpAbstraction.HttpResponseVO;

 
public class HttpUrlConnction {
 
	private final String USER_AGENT = "Mozilla/5.0";

	// HTTP GET request
	public HttpResponseVO sendGet( String siteUrl) throws Exception {
 		String url = siteUrl;
//                url = URLEncoder.encode("URL", "UTF-8");
                HttpResponseVO responseVO = new HttpResponseVO();
 		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
 		// optional default is GET
		con.setRequestMethod("GET");
 		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);
		responseVO.responseCode = con.getResponseCode();
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
                responseVO.responseBody= response.toString();
                return responseVO ;
 
	}
 
 
}