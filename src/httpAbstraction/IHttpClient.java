package httpAbstraction;


import java.util.Hashtable;

public interface IHttpClient {

    HttpResponseVO post( String                       url,
                       Hashtable<String, String>    headers,
                       String                       requestBody,
                       int                          timeOutMs)
            throws  Exception;

    HttpResponseVO get( String                       url,
                      Hashtable<String, String>    headers,
                      int                          timeOutMs)
            throws  Exception;
}
