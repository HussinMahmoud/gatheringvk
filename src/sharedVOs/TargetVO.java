package sharedVOs;


public class TargetVO {
    
private long id ;
private String first_name ;
private String country;
private String langs ;

    public TargetVO() {
        this.id = 0;
        this.first_name = "";
        this.country = "";
        this.langs = "";
    }




    public void setId(long id) {
        this.id = id;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setLangs(String langs) {
        this.langs = langs;
    }

    public long getId() {
        return id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getCountry() {
        return country;
    }

    public String getLangs() {
        return langs;
    }


   
}
