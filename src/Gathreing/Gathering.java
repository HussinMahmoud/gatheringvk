package Gathreing;

import HttpRequest.HttpUrlConnction;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import httpAbstraction.HttpResponseVO;
import java.util.LinkedList;
import java.util.Random;
import javax.print.DocFlavor;
import sharedVOs.TargetVO;


/**
 *
 * @author HussinMahmoud
 */

public class Gathering {
    private HttpUrlConnction httpConnction;
    private final int DELAY_MIN = 5000;
    private final int DELAY_MIX = 300000;
    private String url ;
    private String[] senders ;
    private String fields ;
    private String endUrl;


    public Gathering() {
        this.httpConnction = new HttpUrlConnction();
        this.url = "https://api.vk.com/method/users.get?";
        this.fields ="fields=country,personal&name_case=Nom&";
        this.endUrl = "v=5.8&access_token=";
        this.senders= new String[3];
        this.senders[0]="232cc7237e2eef1dca46531e82fc7575e14232ae43df9ee63126c3116932183f0fbe72aad6646765842cd";
        this.senders[1]="e8eb6c7ce01aa3d95a640d94c2370aa7016c1c8d129eed2c3cb23a44a4b805efa5b3905202b225379f4ac";
        this.senders[2]="7e50b04ac28ce678d2995673fd6feae59e64cb1136f335decf2bcb87f2fdb6a4d23ffad1af604415d21af";        
        
    }
    
    
    private int randomTimeSleep(){
        Random ra = new Random();
        int timeSleep = ra.nextInt(DELAY_MIX);
        if(timeSleep<0){
            timeSleep *= -1 ;
        }
        
        return timeSleep ;
       
    }

    private HttpResponseVO getResponseVO () throws InstantiationException, IllegalAccessException, ClassNotFoundException, Exception{
        Random ra = new Random();
        int index = ra.nextInt(3);
        index =  (index < 0) ? -index  : index;
        
        String accessToken = this.senders[index];
        StringBuilder urlRequest = new StringBuilder();
        urlRequest.append(this.url);
        urlRequest.append(this.fields);
        String userIds = DbConnecet.oneThousandUsers();
        urlRequest.append("user_ids=" + userIds);
        urlRequest.append(this.endUrl);
        urlRequest.append(accessToken);
        
        HttpResponseVO responseResult = this.httpConnction.sendGet(urlRequest.toString());
        
        return responseResult ;
        
    }
    
    
    private LinkedList<TargetVO> getTargetList(String responseBady) throws ParseException {
       if (responseBady.contains("error_code")){
           
           return null ;
       }
        
        JSONParser jsonParser = new JSONParser();
        JSONObject response = (JSONObject)jsonParser.parse(responseBady);
        JSONArray arrayResult = (JSONArray)response.get("response");
       int size = arrayResult.size();
       LinkedList<TargetVO> TargetList = new LinkedList<TargetVO>();
       
        for (int i = 0; i < size; i++) {
            TargetVO  target = new TargetVO();
            JSONObject element = (JSONObject) arrayResult.get(i);
           
            if (element.get("deactivated")!= null){
                continue;
            }
            
            target.setId((long)element.get("uid"));
            target.setFirst_name((String)element.get("first_name"));
            
         
            if (element.get("country") != null){
                try {
                     long country = (long)element.get("country");
               target.setCountry(country+"");
                } catch (Exception e) {
                }
               
            }
        
            
            if(element.get("personal") != null ){
                Object presonal = null ;
                try {
                  presonal   = (JSONObject)element.get("personal");
                } catch (Exception e) {
                    
                  
                }
               
            if( presonal != null ){
                JSONObject pers = (JSONObject)presonal;
                if (pers.get("langs")!= null){
            JSONArray langs = (JSONArray)  pers.get("langs");
                    try {
                        target.setLangs((long)langs.get(0)+"");
                    } catch (Exception e) {
                    }
                    
                    try {
                         target.setLangs((String)langs.get(0));
                    } catch (Exception e) {
                    }
            
                }
            }
            }
            
            TargetList.add(target);
            
        }
       
        return TargetList ;
        

    }
    
    
    private void insertdb (LinkedList<TargetVO> targetList){
        
        int size = targetList.size();
        boolean result = false ;
        
        for (int i = 0; i < size; i++) {
            TargetVO target = targetList.get(i);
            while (true) { 
                try {
                  result =  DbConnecet.insertData(target.getId(), target.getFirst_name(), target.getCountry(),target.getLangs());
                    if (result) {
                        System.out.println("Insert success target "+target.getFirst_name());
                    }
                  break;
                } catch (Exception e) {
                }
                
                try {
                    Thread.sleep(DELAY_MIN);
                } catch (Exception e) {
                }
                
            }
            
           try {
                    Thread.sleep(100);
                } catch (Exception e) {
                }
        }
        
        
        
        
    }
    


    public static void main(String[] args) throws IllegalAccessException, ClassNotFoundException, Exception {
        Gathering vk = new Gathering ();
        while (true) { 
            HttpResponseVO responseResult = null ;
            try {
                responseResult =  vk.getResponseVO ();
            } catch (Exception e) {
                 System.out.println("error cannot get rsponse ");
                 Thread.sleep(vk.DELAY_MIN);  
                 continue;
                
            }
            
             
             if (responseResult.responseCode != 200) {
                  try {
                      System.out.println("error rsponse code "+ responseResult.responseCode);
                    Thread.sleep(vk.DELAY_MIX);
                    continue;
                } catch (Exception e) {
                }
                
            }
             
            LinkedList<TargetVO> targetList =   vk.getTargetList(responseResult.responseBody);
             
             
             vk.insertdb(targetList);
             
             try {
                 int sleep =vk.randomTimeSleep() ; 
                 System.out.println( "now sleep ms "+ sleep);
                 
               Thread.sleep(sleep);
            } catch (Exception e) {
            }
             
        }
       
        
        
        
        
        
        
        
    }
}
